#!/bin/bash

# TODO: make CTRL-C work

ct=0
while [[ $ct -ge 0 ]]
do
    ping -c 1 8.8.8.8 1&>/dev/null \
    \
    && echo SUCCESS \
    && aplay ~/od/.rsrc/sounds/freesound/SilverIllusionist/notification-retro-vibes.wav \
    && date \
    && ct=-1 \
    \
    || ct=$((ct+1)) \
    && echo $ct
done
