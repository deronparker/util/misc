use std::io::stdin;

use regex::Regex;

fn main() {
    let re = Regex::new(r#"<div .*?class=".*?TermText.*?".*?>(?<front>.*?)<\/span><\/div>.*?<div .*?class=".*?TermText.*?".*?>(?<back>.*?)<\/span>.*?<\/div>"#).unwrap();

    let input_without_linebreaks: String = stdin().lines().map(|res| res.unwrap()).collect();

    let term_sets = re
        .captures_iter(&input_without_linebreaks)
        .map(|caps| (caps.name("front").unwrap(), caps.name("back").unwrap()));

    for pair in term_sets {
        println!("## {}", pair.0.as_str());
        println!("{}", pair.1.as_str());
        println!();
    }
}
